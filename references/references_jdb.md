## jdb

---

### rc_printer 

Ce projet se place dans le cadre de mon DNAT à l'ESAD Valence
Ma réflexion se portait sur l'étude des systèmes matriciels, de l'écran à l'impression
Je suis passé par une phase exploratoire très bidouille où je construisais des petites machines qui imprimaient
Celle-ci c'est une imprimante à roulette
A roulette parce que comme ça, de manière théorique, on peut imprimer sur n'importe quelle surface plane puisque ce n'est plus la surface mais la machine qui se déplace
1 interface de contribution en ligne où les gens pouvaient simplement dessiner en carré noir et blanc dans une matrice contrainte de n x n
et ensuite les images étaient converties en texte, puis converties en nombre de pas et en position (levé/descendu) pour la machine
J'aime cette phase de conception, où je dessine des machines pour tester la viabilité / faisabilité des choses
J'aime quand la chose s'active, ce côté spectacle de la chose en train de se faire
J'aime l'aspect rudimentaire, un peu shlag des choses, qui donne sa forme à la preuve de concept pour la confirmer ou non

---

### travelling

Ce projet en fait s'est déroulé juste à la suite d'une exposition que j'ai faite avec Eric Watier en 2015
On s'était donné 1 mois in situ, dans la galerie aperto qu'on nous avait prété.
On a acheté des machines d'occase, on les a éprouver en imprimant des images qui trainaient sur nos disques (photos de nuit à porquerolles, capture d'écran de fallout newvegas)
On les a mis en scène dans l'espace, avec leur pauvre facture, qui parlait finalement plus de la machine comme sujet plutôt que de l'image.
Et à la fin, j'ai fabriqué une petite vitre que je pouvais déplacer pour pouvoir avec un scanner à main, passer sur tout volume (objet, cylindre, rame)
Du coup ça donne un fichier .jpg de toute l'exposition, mis en page dans un livre format A4, comme une ligne (de page en page) imprimé sur place (2 exemplaires)
Pour info l'entiereté du livre se retrouve dans *Bloc 2* d'Eric paru l'année dernière

---

### posters

Ces 2 posters sont un peu liés parce qu'il comporte tous les deux des matrices de points
Le premier c'est la synthèse de toutes les contributions du projet RC_printer, sous leur forme de texte, ou avec un peu de distance on aperçoit les images
J'aime bien cette relation texte / image (il y a richard Kostelanetz qui parle d'*imextes*, j'aime bien même si je connais pas trop son boulot)
Le second comporte de la couleur, de manière aléatoire, et les nuages qui se forment sont en fait l'inscription textuelle de chaque millimètre de la page, dans le sens de lecture occidental
J'aime quand le texte ça crée des choses un peu plus de type matériel voire matériau, comme un super calligramme d'aujourd'hui, et que la lecture se fassent autrement qu'avec le texte

---

### srt (oral, sans images)

Là je vais vous expliquer un projet que je suis en train de développer quand j'ai un peu de temps
C'est un lecteur de sous-titre, très classique à la base, sauf que :
	- j'enlève la vidéo
	- je mets plusieurs films en même temps
	- je fais de la face detection sur les vidéos qui sont pas affichés pour positionner le texte dans la page
Le texte se dessine en noir dans le blanc, créant un espace sensé dans la page.
Je pense qu'il y a une attention particulière à attribuer au choix des films à afficher en simultané pour activer le caractère oeuvre de ce truc

---

### écrans signaux (oral, sans images)

Je crée un espace virtuel (qui est invisible a priori) d'une pièce avec ses mesures
J'équipe des écrans (ipad) d'un chariot qui est capté par un appareillage, dont les données sont envoyés sur un serveur local (node.js)
des vidéos sont attribués à la géométrie de la pièce, et selon la positio ndu chariot écran dans l'espace, il capte plus ou moins l'une ou l'autre vidéo, selon une intensité
sur l'écran est rendu ce morceau *signal* de la vidéo, avec tous les autres captés et est reconstituée une espèce de vidéo composite de tous ces signaux, que l'on reconnait ou pas
Je sais pas du tout quelle image ça peut créer et c'est pour ça que ce projet m'intéresse
Ne pas savoir ce que va être le résultat, au fond c'est un peu ça l'expérimentation, le côté expérimental de ma pratique
Un autre point que j'aime bien c'est le positionnement au sol des écrans, la violence du coup de pied pour déplacer les chariots, qui met le spectateur dans l'action directe
Ce point de tension entre corps et écran (parler de la colonne vertebrale), qui questionne la matérialité de tous les espaces, avec le corps comme instrument de mesure

---

