# Acid Tests

Un test à l'acide désigne communément une méthode d'identification chimique
simple des matériaux métalliques à base d'acide, et en particulier dans le cadre
historique de la ruée vers l'or, l'utilisation d'un acide fort pour distinguer
l'or pur des autres métaux communs. 

Le test historique consiste à verser une goutte d'un acide fort, comme de
l'acide nitrique, sur la surface d'un métal à identifier. S'il s'agit d'un métal
noble, il ne sera pas affecté, sinon les traces de l'attaque acide se
manifestent immédiatement sous forme de mousse ou de bulles. Les acheteurs d'or
pratiquaient tous ce test immédiat, bon marché, facile à réaliser et
déterminant.

Par extension, on appelle test à l'acide toute méthode de caractérisation
simple et supposée définitive d'une personne ou d'un produit — tout test
immédiat et caractéristique.

Le terme fut utilisé pour designer une série de fête tenu par l'author Ken Kesey
principalement dans la région de la baie de San Francisco au milieu des années
1960, entièrement centrées sur l'utilisation et la promotion de la drogue
psychédélique LSD, aussi appelée "acide". Le LSD n'est devenu illégal en
Californie que le 6 octobre 1966. 

Il s'agissait de tester son propre caractère. En effet, le LSD pratiquerait
une sorte de dissociation entre soi-même et le monde extérieur, qui permet
d'"être vrai" pendant la durée des effets.

Le terme est aussi utilisé en analyse financière. la méthode du quick ratio
surnommer acid test permet d'évaluer la liquidité d'une entreprise. 

Enfin le test des standards W3C par les navigateurs Internet s'appelle
l'Acid test (1,2,3)

Le premier *Acid test* pour navigateur web a été développé en octobre 1998 et a
joué un rôle important dans l'établissement d'une interopérabilité de base entre
les premiers navigateurs web, en particulier pour la spécification des feuilles
de style de niveau 1 (CSS 1.0). 

Tout comme les tests à l'acide pour l'or qui permettent une évaluation rapide et
évidente de la qualité d'une pièce de métal, les *Acid Tests* sur le web ont été
conçus pour fournir une indication claire de la conformité d'un navigateur aux
normes web. 

tests, épreuve, défi, (non)standard, (non)conforme, possibilités, limites,
vérité, outils, école, étudiants

__Le printLab peut-il passer l'*acid test* ?__
