`2017`  
**Maximage**, 
*Raster Guide, Color Guide, Color Combinations*  
<https://maximage.biz/projects/maximage-color-combinations#no-8>

---

`2016`  
**&Eacute;milie Fayet**, 
*Mémoire sur les couleurs IGC*  
<http://dnsep.emiliefayet.fr/>

---

`2015`  
**La Villa Hermosa**, 
*Homéopape*  
<http://blog.lavillahermosa.com/homeopape-nuit-blanche/>

---

`2012`  
**AND publishing & Abake**, 
*Variable Formats*  
<http://andpublishing.org/variable-formats-2/>

---

`2011`  
**Silvio Lorusso avec Giulia Ciliberto**, 
*Blank on demand*  
<https://silviolorusso.com/work/blank-on-demand/>

---

`2008`  
**Jurg Lehni & Alex Rich**, 
*Empty Words*  
<http://juerglehni.com/works/empty-words>

---

`2008`  
**James Goggin, avec des étudiants de la faculté de design de la University of Applied Sciences Darmstadt & Prof. Frank Philippin**, 
*Dear Lulu*  
<http://p-dpa.net/work/dear-lulu/>

---

`2010`  
**Xavier Antin**, 
*01_Just in Time, or a Short History of Production*  
<http://xavierantin.fr/>

---

`2010`  
**Xavier Antin**, 
*Printing at home*  
<http://xavierantin.fr/>

---

`1913–14, replica 1964`  
**Marcel Duchamp**, 
*3 stoppages étalons*  
<https://www.centrepompidou.fr/cpv/resource/crb5LdB/r5x8xd>

---

`xxxx`  
*Morgane Bartoli*, 
*OP003, La numérisation tue l'image*,  
<https://objetpapier.fr/op-release/op003-la-numerisation-tue-limage.html>
